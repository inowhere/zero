#!/bin/bash

# Step 1: Set up Virtual Environment
echo "Step 1: Setting up virtual environment..."
python3 -m venv venv
source venv/bin/activate

# Step 2: Install Dependencies
echo "Step 2: Installing required dependencies..."
pip install -r requirements.txt

# Step 3: Configure Database
echo "Step 3: Configuring PostgreSQL database credentials..."
# Modify app.py with your PostgreSQL database credentials

# Step 4: Run the Application
echo "Step 4: Running the application..."
python app.py

# Step 5: Access the Application
echo "Step 5: Opening the application in your web browser..."
url="http://localhost:5000"
if command -v open > /dev/null; then
    open $url
elif command -v xdg-open > /dev/null; then
    xdg-open $url
else
    echo "Unable to open a web browser automatically. Please open your web browser and visit $url."
fi

