# app.py
import os
import psycopg2
from psycopg2 import pool
from dotenv import load_dotenv
from werkzeug.utils import secure_filename
from flask import Flask, render_template, send_from_directory, request, redirect, url_for, g

load_dotenv()

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = 'static/uploads'
app.config['ALLOWED_EXTENSIONS'] = {'png', 'jpg', 'jpeg', 'gif'}

app.config['DATABASE'] = {
    'host': os.environ.get('DB_HOST', 'default_value_if_not_set'),
    'database': os.environ.get('DB_NAME', 'default_value_if_not_set'),
    'user': os.environ.get('DB_USER', 'default_value_if_not_set'),
    'password': os.environ.get('DB_PASSWORD', 'default_value_if_not_set')
}


# Connection pool initialization
db_pool = None

def init_db_pool():
    global db_pool
    db_pool = pool.ThreadedConnectionPool(
        1,  # minimum number of connections
        20, # maximum number of connections
        host=app.config['DATABASE']['host'],
        database=app.config['DATABASE']['database'],
        user=app.config['DATABASE']['user'],
        password=app.config['DATABASE']['password']
    )


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

def connect_db():
    if not hasattr(g, 'db_conn'):
        g.db_conn = db_pool.getconn()
    return g.db_conn

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        image = request.files['image']
        description = request.form['description']

        if image and allowed_file(image.filename):
            filename = secure_filename(image.filename)
            image_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            image.save(image_path)

            # inserting a new entry
            try:
                conn = connect_db()
                cur = conn.cursor()
                cur.execute('INSERT INTO entries (image_path, description) VALUES (%s, %s)', (image_path, description))
                conn.commit()
            except psycopg2.DatabaseError as e:
                print(f"Database error: {e}")
                # maybe return a 500 error to the user
            finally:
                if cur:
                    cur.close()
                if conn:
                    db_pool.putconn(conn)

            return redirect(url_for('index'))

    # fetching entries
    try:
        conn = connect_db()
        cur = conn.cursor()
        cur.execute('SELECT * FROM entries ORDER BY id DESC')
        entries = cur.fetchall()
    except psycopg2.DatabaseError as e:
        print(f"Database error: {e}")
        # maybe return a 500 error to the user
    finally:
        if cur:
            cur.close()
        if conn:
            db_pool.putconn(conn)

    return render_template('index.html', entries=entries)

# Route to serve static files (CSS, JS, images, etc.):
@app.route('/static/<path:filename>')
def serve_static(filename):
    return send_from_directory('static', filename)

# Route for editing an entry (text)
@app.route('/edit/<int:id>', methods=['POST'])
def edit_entry(id):

    # Get the new text from the request data
    data = request.json
    new_text = data.get('text', '').strip()

    try:
        conn = connect_db()
        cur = conn.cursor()
        # Update the text in the database
        cur.execute('UPDATE entries SET description = %s WHERE id = %s', (new_text, id))
        conn.commit()
    except psycopg2.DatabaseError as e:
        print(f"Database error: {e}")
        # maybe return a 500 error to the user
    finally:
        if cur:
            cur.close()
        if conn:
            db_pool.putconn(conn)

    return redirect(url_for('index'))

# Route for deleting an entry (both image and text)
@app.route('/delete/<int:id>', methods=['POST'])
def delete_entry(id):
    try:
        conn = connect_db()
        cur = conn.cursor()
        
        # Get the image path from the database using the provided ID
        cur.execute('SELECT image_path FROM entries WHERE id = %s', (id,))
        image_path = cur.fetchone()[0]
        
        # Delete the image file from the server
        if os.path.exists(image_path):
            os.remove(image_path)

        # Delete the entry from th e database
        cur.execute('DELETE FROM entries WHERE id = %s', (id,))
        conn.commit()
    except psycopg2.DatabaseError as e:
        print(f"Database error: {e}")
        # maybe return a 500 error to the user
    finally:
        if cur:
            cur.close()
        if conn:
            db_pool.putconn(conn)

    return redirect(url_for('index'))

if __name__ == '__main__':
    init_db_pool()
    app.run(debug=True)

